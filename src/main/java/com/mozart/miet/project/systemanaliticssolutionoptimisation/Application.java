package com.mozart.miet.project.systemanaliticssolutionoptimisation;

import com.mozart.math.matrixandstats.matrix.MatrixUtils;
import com.mozart.miet.project.systemanaliticssolutionoptimisation.barcode.BarcodeReader;
import com.mozart.miet.project.systemanaliticssolutionoptimisation.matrixandstats.MatrixAndStatExample;
import com.mozart.miet.project.systemanaliticssolutionoptimisation.simplex.SimplexPlanning;
import com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.PrintType;
import com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.UsefulUtils;

import java.io.IOException;

import static com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.UsefulUtils.*;

/**
 * <p>
 * Main class for project
 * </p>
 * miet-project. Created: 15.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class Application {

    private static final String CONFIG_PATH =
            "C:\\Documents\\Projects\\MIET\\system-analitics-solution-optimisation\\src\\main\\resources\\config\\config.txt";
    private static final long START_TIME = System.currentTimeMillis();

    public static void main(String[] args) {
        try {
            init(args.length > 0 ? args[0] : CONFIG_PATH);
            startProgram();
        } catch (IOException e) {
            print(PrintType.ERROR, "Конфиг файл не найден");
        } finally {
            print(PrintType.INFO, "Время выполнения: " + (System.currentTimeMillis() - START_TIME) + " мс");
            print(PrintType.INFO, "Финал");
        }
    }

    private static void startProgram() {
        final String executeMatrix = getParam("execute.matrix");
        if (executeMatrix != null && Boolean.valueOf(executeMatrix)) {
            new MatrixAndStatExample().start();
        }
        final String executeBarcode = getParam("execute.barcode");
        if (executeBarcode != null && Boolean.valueOf(executeBarcode)) {
            new BarcodeReader().start();
        }
        final String executeSimplex = getParam("execute.simplex");
        if (executeSimplex != null && Boolean.valueOf(executeSimplex)) {
            try {
                new SimplexPlanning().start();
            } catch (Exception e) {
                print(PrintType.ERROR, e.getMessage());
            }
        }
    }

    private static void init(String configPath) throws IOException {
        config = UsefulUtils.getConfig(configPath);
        MatrixUtils.setIndexStr(getParam("logging.format"));
        print(PrintType.INFO, "Старт");
    }
}