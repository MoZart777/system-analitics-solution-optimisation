package com.mozart.miet.project.systemanaliticssolutionoptimisation.simplex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.UsefulUtils.getParam;

public class SimplexPlanning {

    private final int k;
    private final int n;

    private final int factorNumbers;
    private final int resultNumbers;

    private final double x10;
    private final double x20;
    private final double i10;
    private final double i20;
    private final List<List<Double>> xFactors;
    private final List<List<Double>> yResults;


    public SimplexPlanning() throws IOException {
        List<String> list = Files.readAllLines(Paths.get(getParam("simplex.data")));
        if (list.size() != 10) {
            throw new RuntimeException("Неверная размерность");
        }

        this.k = Integer.valueOf(list.get(0));
        this.n = (int) Math.pow(2, k);
        list.remove(0);

        Integer[] factAndResNumbers = Arrays.stream(list.get(0).split(" "))
                .map(Integer::valueOf).toArray(Integer[]::new);
        this.factorNumbers = factAndResNumbers[0];
        this.resultNumbers = factAndResNumbers[1];
        list.remove(0);

        List<List<Double>> xFactors = new ArrayList<>(factorNumbers);
        for (int i = 0; i < factorNumbers; i++) {
            xFactors.add(Arrays.stream(list.get(0).split(" "))
                    .map(Double::valueOf).collect(Collectors.toList()));
            list.remove(0);
        }
        this.xFactors = xFactors;

        List<List<Double>> yResults = new ArrayList<>(resultNumbers);
        for (int i = 0; i < resultNumbers; i++) {
            yResults.add(Arrays.stream(list.get(0).split(" "))
                    .map(Double::valueOf).collect(Collectors.toList()));
            list.remove(0);
        }
        this.yResults = yResults;

        Double[] xInit = Arrays.stream(list.get(0).split(" "))
                .map(Double::valueOf).toArray(Double[]::new);
        this.x10 = xInit[0];
        this.x20 = xInit[1];
        Double[] iInit = Arrays.stream(list.get(1).split(" "))
                .map(Double::valueOf).toArray(Double[]::new);
        this.i10 = iInit[0];
        this.i20 = iInit[1];
    }


    public void start() {
        List<Double> yResultAverage = new ArrayList<>(factorNumbers);
        for (int x = 0; x < factorNumbers; x++) {
            Double average = 0.0;
            for (int y = 0; y < resultNumbers; y++) {
                average += yResults.get(y).get(x);
            }
            yResultAverage.add(average);
        }
        // TODO: 19.12.2018 неоптимизированный говнокод
        double b0 = yResultAverage.parallelStream().mapToDouble(c -> c).sum() / n;
        double b1 = getBWithOne(0, yResultAverage);
        double b2 = getBWithOne(1, yResultAverage);
        double b3 = getBWithOne(2, yResultAverage);
        double b12 = getBWithTwo(0, 1, yResultAverage);
        double b13 = getBWithTwo(0, 2, yResultAverage);
        double b23 = getBWithTwo(1, 2, yResultAverage);
        double b123 = getBWithThree(0, 1, 2, yResultAverage);

        List<Double> bList = Arrays.asList(b0, b1, b2, b3, b12, b13, b23, b123);

        List<Double> sJ = new ArrayList<>(n);

        for (int y = 0; y < n; y++) {
            final Double yAverage = yResultAverage.get(y);
            sJ.add(yResults.get(y).parallelStream()
                    .map(c -> Math.pow(c - yAverage, 2))
                    .mapToDouble(c -> c).sum() / (2));
        }

        Double sJSummary = sJ.parallelStream().mapToDouble(c -> c).sum() / bList.size();




    }

    private double getBWithOne(int xIndex, List<Double> resultAverage) {
        double b = 0;
        for (int i = 0; i < n; i++) {
            b += xFactors.get(xIndex).get(i) * resultAverage.get(i);
        }
        return b / n;
    }

    private double getBWithTwo(int xIndex1, int xIndex2, List<Double> resultAverage) {
        double b = 0;
        for (int i = 0; i < n; i++) {
            b += xFactors.get(xIndex1).get(i) * xFactors.get(xIndex2).get(i) * resultAverage.get(i);
        }
        return b / n;
    }

    private double getBWithThree(int xIndex1, int xIndex2, int xIndex3, List<Double> resultAverage) {
        double b = 0;
        for (int i = 0; i < n; i++) {
            b += xFactors.get(xIndex1).get(i)
                    * xFactors.get(xIndex2).get(i)
                    * xFactors.get(xIndex3).get(i)
                    * resultAverage.get(i);
        }
        return b / n;
    }
}
