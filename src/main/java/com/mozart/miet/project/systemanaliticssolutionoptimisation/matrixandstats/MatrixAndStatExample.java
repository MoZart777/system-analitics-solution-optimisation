package com.mozart.miet.project.systemanaliticssolutionoptimisation.matrixandstats;

import com.mozart.math.matrixandstats.matrix.Matrix;
import com.mozart.math.matrixandstats.matstat.MatStatRandomVariable;
import com.mozart.math.matrixandstats.matstat.MatStatRandomVariables;
import com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.PrintType;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.mozart.math.matrixandstats.matrix.MatrixUtils.matrixToString;
import static com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.UsefulUtils.*;

public class MatrixAndStatExample {

    public void start() {
        startDet();
        startTransporated();
        startRevert();
        startSumAndDifference();
        startMultiplication();
        startDivision();
        startMathExpectationAndDispersion();
        startMathCorrelation();
    }

    private void startDet() {
        try {
            List<String> list = Files.readAllLines(Paths.get(getParam("matrix.det")));
            Double[][] data = listToArray(list);
            printObjectsWithInfoLog(
                    "=== Детерминант матрицы ===",
                    "Исходная:", matrixToString(data),
                    "Детерминант равен: " + new Matrix(data).getDet());
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startTransporated() {
        try {
            List<String> list = Files.readAllLines(Paths.get(getParam("matrix.transporated")));
            Double[][] data = listToArray(list);
            printObjectsWithInfoLog(
                    "=== Транспонированная матрица ===",
                    "Исходная:", matrixToString(data),
                    "Результат:", matrixToString(new Matrix(data).getTransporatedMartix()));
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startRevert() {
        try {
            List<String> list = Files.readAllLines(Paths.get(getParam("matrix.revert")));
            Double[][] data = listToArray(list);
            printObjectsWithInfoLog(
                    "=== Обратная матрица ===",
                    "Исходная:", matrixToString(data),
                    "Результат:", matrixToString(new Matrix(data).getRevertMatrix()));
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startSumAndDifference() {
        try {
            List<String> list1 = Files.readAllLines(Paths.get(getParam("matrix.sum1")));
            List<String> list2 = Files.readAllLines(Paths.get(getParam("matrix.sum2")));
            Double[][] data1 = listToArray(list1);
            Double[][] data2 = listToArray(list2);
            printObjectsWithInfoLog(
                    "=== Сумма и разность матриц ===",
                    "Исходная 1:", matrixToString(data1),
                    "Исходная 2:", matrixToString(data2),
                    "Сумма:", matrixToString(new Matrix(data1).sum(new Matrix(data2))),
                    "Разность:", matrixToString(new Matrix(data1).difference(new Matrix(data2))));
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startMultiplication() {
        try {
            List<String> list1 = Files.readAllLines(Paths.get(getParam("matrix.multiplication1")));
            List<String> list2 = Files.readAllLines(Paths.get(getParam("matrix.multiplication2")));
            Double[][] data1 = listToArray(list1);
            Double[][] data2 = listToArray(list2);
            printObjectsWithInfoLog(
                    "=== Произведение матриц ===",
                    "Исходная 1:", matrixToString(data1),
                    "Исходная 2:", matrixToString(data2),
                    "Результат:", matrixToString(new Matrix(data1).multiplication(new Matrix(data2))));
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startDivision() {
        try {
            List<String> list1 = Files.readAllLines(Paths.get(getParam("matrix.division1")));
            List<String> list2 = Files.readAllLines(Paths.get(getParam("matrix.division2")));
            Double[][] data1 = listToArray(list1);
            Double[][] data2 = listToArray(list2);
            printObjectsWithInfoLog(
                    "=== Частное матриц ===",
                    "Исходная 1:", matrixToString(data1),
                    "Исходная 2:", matrixToString(data2),
                    "Результат:", matrixToString(new Matrix(data1).division(new Matrix(data2))));
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startMathExpectationAndDispersion() {
        try {
            List<String> list = Files.readAllLines(Paths.get(getParam("math.expectation.dispersion")));
            Double[] data1 = stringToArray(list.get(0));
            Double[] data2 = stringToArray(list.get(1));
            printObjectsWithInfoLog(
                    "=== Мат ожидание и дисперсия ===",
                    "Случайная величина:", list.get(0),
                    "Вероятности:", list.get(1),
                    "Мат ожидание:", new MatStatRandomVariable(data1, data2).getMathExpectation(),
                    "Дисперсия:", new MatStatRandomVariable(data1, data2).getDispersion());
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void startMathCorrelation() {
        try {
            List<String> list = Files.readAllLines(Paths.get(getParam("math.correlation")));
            Double[] data1 = stringToArray(list.get(0));
            Double[] data2 = stringToArray(list.get(1));
            printObjectsWithInfoLog(
                    "=== Корреляция ===",
                    "Случайная величина 1:", list.get(0),
                    "Случайная величина 2:", list.get(1),
                    "Результат:", new MatStatRandomVariables(data1, data2).getCorrelation());
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }
}
