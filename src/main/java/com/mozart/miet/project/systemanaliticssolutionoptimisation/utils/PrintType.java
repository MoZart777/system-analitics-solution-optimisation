package com.mozart.miet.project.systemanaliticssolutionoptimisation.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Print enum
 * </p>
 * miet-project. Created: 15.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public enum PrintType {

    DEBUG(PrintType.DEBUG_MESSAGE + ": ", 0),
    INFO(PrintType.INFO_MESSAGE + ": ", 1),
    WARN(PrintType.WARN_MESSAGE + ": ", 2),
    ERROR(PrintType.ERROR_MESSAGE + ": ", 3);

    private static final String DEBUG_MESSAGE = "DEBUG";
    private static final String INFO_MESSAGE = "INFO";
    private static final String WARN_MESSAGE = "WARN";
    private static final String ERROR_MESSAGE = "ERROR";


    private final String type;
    private final Integer weight;

    PrintType(String type, Integer weight) {
        this.type = type;
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public Integer getWeight() {
        return weight;
    }

    public static PrintType getFromString(String type) {
        if (StringUtils.isBlank(type)) {
            return null;
        }
        switch (type) {
            case DEBUG_MESSAGE:
                return DEBUG;
            case INFO_MESSAGE:
                return INFO;
            case WARN_MESSAGE:
                return WARN;
            case ERROR_MESSAGE:
                return ERROR;
            default:
                return null;
        }
    }
}
