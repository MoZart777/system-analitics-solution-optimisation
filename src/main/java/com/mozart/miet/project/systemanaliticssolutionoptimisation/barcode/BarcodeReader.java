package com.mozart.miet.project.systemanaliticssolutionoptimisation.barcode;

import com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.PrintType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.UsefulUtils.getParam;
import static com.mozart.miet.project.systemanaliticssolutionoptimisation.utils.UsefulUtils.print;

public class BarcodeReader {

    private final Integer BLACK = 0;
    private final Integer ZERO = -1;

    private final String parentFolderPath;
    private final String resultFolderPath;
    private final String resultBarcodeFileSuffix;

    public BarcodeReader() {
        this.parentFolderPath = getParam("barcode.data.folder");
        this.resultFolderPath = getParam("barcode.result.folder");
        this.resultBarcodeFileSuffix = getParam("barcode.result.file.suffix");
    }

    public void start() {
        try {
            barcodeRead();
        } catch (Exception e) {
            print(PrintType.WARN, e.getMessage());
        }
    }

    private void barcodeRead() throws IOException {
        File parentFolder = new File(parentFolderPath);
        if (!parentFolder.isDirectory()) {
            throw new RuntimeException(parentFolderPath + "is not a file");
        }

        final int optimum = -10007216;

        for (File file : Objects.requireNonNull(parentFolder.listFiles())) {
            final String fileName = file.getName();
            BufferedImage image = ImageIO.read(file);
            int width = image.getWidth();
            int height = image.getHeight();
            List<Column> list = new ArrayList<>(width);

            for (int x = 0; x < width; x++) {
                Integer[] column = new Integer[height];
                for (int y = 0; y < height; y++) {
                    column[y] = image.getRGB(x, y);
                }
                list.add(new Column(column, x));
            }

            BufferedImage outImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            list.forEach(l -> {
                int xIndex = l.getIndex();
                if (Arrays.stream(l.getRow()).map(a -> a <= optimum ? 1 : -1).mapToInt(e -> e).sum() >= 0) {
                    for (int y = 0; y < height; y++) {
                        outImage.setRGB(xIndex, y, BLACK);
                    }
                } else {
                    for (int y = 0; y < height; y++) {
                        outImage.setRGB(xIndex, y, ZERO);
                    }
                }
            });

            File outFile = new File(resultFolderPath + resultBarcodeFileSuffix + fileName);
            ImageIO.write(outImage, StringUtils.substringAfterLast(fileName, "."), outFile);
            print(PrintType.INFO, "Файл " + fileName + " успешно обработан");
        }
    }

    @Getter
    public static class Column {

        private Integer[] row;
        private final Integer index;

        Column(Integer[] row, Integer index) {
            this.row = row;
            this.index = index;
        }
    }
}
