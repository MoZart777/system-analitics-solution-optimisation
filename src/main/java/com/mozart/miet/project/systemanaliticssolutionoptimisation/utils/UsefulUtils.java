package com.mozart.miet.project.systemanaliticssolutionoptimisation.utils;

import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * <p>
 * Useful utils
 * </p>
 * miet-project. Created: 15.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class UsefulUtils {

    public static Map<String, String> config;

    private static final String SPACE = " ";
    private static PrintType printLvl = PrintType.DEBUG;
    private static boolean fileLogging = false;
    private static String logFilePath = null;
    private static Path file;

    public static Double[][] listToArray(List<String> list) {
        return list.stream().filter(Objects::nonNull)
                .map(s -> Arrays.stream(s.split(SPACE))
                        .map(Double::valueOf).toArray(Double[]::new))
                .toArray(Double[][]::new);
    }

    public static Double[] stringToArray(String string) {
        return Arrays.stream(string.split(SPACE))
                .map(Double::valueOf).toArray(Double[]::new);
    }

    public static void printObjectsWithInfoLog(Object... objects) {
        for (Object o : objects) {
            print(PrintType.INFO, o);
        }
    }

    public static void print(PrintType type, Object text) {
        if (type.getWeight() >= printLvl.getWeight()) {
            System.out.println(type.getType() + "\t" + text);
        }
        if (fileLogging) {
            try {
                Files.write(file, Collections.singleton(Calendar.getInstance().getTime().toString()
                                + " " + type.getType() + text.toString()),
                        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            } catch (IOException e) {
                System.out.print(PrintType.ERROR.getType() + " Ошибка логгирования в файл");
            }
        }
    }

    public static Map<String, String> getConfig(String configPath) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(configPath));
        ConcurrentMap<String, String> res = new ConcurrentHashMap<>();
        lines.parallelStream().forEach(l -> {
            if (StringUtils.isNotBlank(l)) {
                String key = StringUtils.substringBefore(l, "=");
                String value = StringUtils.substringAfter(l, "=");
                if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value) && !key.startsWith("#")) {
                    res.put(key, value);
                    switch (key) {
                        case "log.console.lvl":
                            printLvl = PrintType.getFromString(value);
                            break;
                        case "log.file.path":
                            logFilePath = value;
                            break;
                        case "log.file.enable":
                            fileLogging = Boolean.valueOf(value);
                            break;
                    }
                }
            }
        });
        postInitConfig();
        return res;
    }

    private static void postInitConfig() {
        if (fileLogging) {
            if (logFilePath == null) {
                print(PrintType.WARN, "Укажите путь для лог файла");
                fileLogging = false;
            } else {
                file = Paths.get(logFilePath);
            }
        }
    }

    public synchronized static String getParam(@NotNull String key) {
        return config.get(key);
    }
}